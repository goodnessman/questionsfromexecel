/* global malarkey:false, moment:false */
(function() {
	'use strict';

	angular
	.module('application')
	.constant('malarkey', malarkey)
	.constant('moment', moment)
	.constant('config', {
		api: {
			questions: 'http://localhost.exeltests/getQuestions.php',
			openQuestions: 'http://localhost.exeltests/getOpenQuestions.php',
			setResult: 'http://localhost.exeltests/addResultToExcel.php',
			password: 'http://localhost.exeltests/getPassword.php',
			questionsCount: 'http://localhost.exeltests/getQuestionsCount.php'
		}
	});

})();
