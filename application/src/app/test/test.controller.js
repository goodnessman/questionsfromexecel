(function() {
	'use strict';

	angular
	.module('application')
	.controller('TestController', TestController);

	/** @ngInject */
	function TestController($scope, $state, MainService, MainRestService) {
		var vm = this,
			currectAnsvers = 0;

        vm.user = MainService.getUser();
		vm.questionsCount = vm.user.qustionsCount;
		vm.currentAnsver = null;
		vm.tests = MainService.getQuestions();
		vm.testNumber = 0;
		vm.currentTest = vm.tests[vm.testNumber];
		vm.checked = true;
		vm.answerField = '';

		$scope.$on('timer-stopped', function (event, data){
            $state.go('results');
        });

		if (!vm.tests.length) {
			$state.go('login'); 
		}

		vm.nextQuestion = function () {
			vm.checked = true;
			if (vm.currentTest.type === 'variant') {
				console.log('vm.currentTest.ansver', vm.currentTest.ansver );
				console.log('vm.currentAnsver', vm.currentAnsver);
				if (vm.currentTest.ansver == vm.currentAnsver) {
					currectAnsvers++;
					MainService.setCorrectAnsvers(currectAnsvers);
				}
			}else if (vm.currentTest.type === 'open') {
				MainService.setOpenQustionAnsver(vm.currentAnsver);
			}
			
			// debugger;
			if (vm.testNumber < (vm.tests.length - 1) && (vm.testNumber + 1) < (vm.questionsCount + 5)) {
				vm.testNumber = vm.testNumber + 1;
				vm.currentTest = vm.tests[vm.testNumber];
				vm.answerField = '';
			}else {
				$state.go('results');
				vm.answerField = '';
			}
		};

		vm.setAnsver = function (val) {
			vm.currentAnsver = val;
			vm.checked = false;
		};
	}
})();



