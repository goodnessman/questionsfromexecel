(function() {
	'use strict';

	angular
	.module('application')
	.controller('LoginController', LoginController);

	/** @ngInject */
	function LoginController($state, MainService, MainRestService, LoginService) {
		var vm = this,
			password = '',
			questionsCount = 0,
			variantQustions = [],
			variantQustionsCount = 0,
			openQuestions = [];

		vm.name = '';
		vm.course = '';
		vm.password = '';

		MainRestService.getQuestionsCount()
		.then(function (data) {
			questionsCount = data;

			MainRestService.getTests()
			.then(function (data) {
				variantQustions = LoginService.parseTests(data);
				variantQustions = LoginService.limiteQuestions(variantQustions, questionsCount);
				variantQustionsCount = variantQustions.length;
			})
			.then(function () {
				MainRestService.getOpenQuestions()
				.then(function (data) {
					openQuestions = LoginService.parseOpenQuestions(data);
				})
				.then(function () {
					var newQuestionsArr = LoginService.addOpenQuestions(openQuestions, variantQustions);
						MainService.setQuestions(newQuestionsArr);
						// console.log(MainService.getQuestions());
				});
			});
		});

		MainRestService.getPassword()
		.then(function (data) {
			password = data;
		});

		

		vm.startTest = function() {
			var user = {};

			user.name = vm.name;
			user.course = vm.course;
			user.password = vm.password;
			user.correctAnsvers = 0;
			user.qustionsCount = questionsCount;
			user.variantQustionsCount = variantQustionsCount;
			user.openQuestionsAnsvers = [];

			if (user.name && user.course && password && user.password == password) {
				MainService.setUser(user);
				$state.go('test');
			}
		};
	}
})();


