<?php 

date_default_timezone_set('America/Los_Angeles');

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT');

/** Include PHPExcel */
require_once 'PHPExcel/Classes/PHPExcel.php';
require_once 'PHPExcel/Classes/PHPExcel/IOFactory.php';

$obj = $_POST['myData'];

$objPHPExcel = PHPExcel_IOFactory::load("results.xlsx");
$objPHPExcel->setActiveSheetIndex(0);
$row = $objPHPExcel->getActiveSheet()->getHighestRow()+1;
//echo $row;
$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, $obj['name']);
$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $obj['course']);
$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $obj['correctAnsvers']);
$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $obj['openQuestionsAnsvers'][0]);
$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $obj['openQuestionsAnsvers'][1]);
$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, $obj['openQuestionsAnsvers'][2]);
$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, $obj['openQuestionsAnsvers'][3]);
$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, $obj['openQuestionsAnsvers'][4]);
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$objWriter->save('results.xlsx');
?>